Install role 
------------
```yaml
- name: nodejs
  src: git+https://gitlab.com/galaxy-roles1/nodejs-galaxy-role.git
  version: v1
```

Example Playbook
----------------
```yaml
- hosts: servers
  gather_facts: true
  become: true
  roles:
    - nodejs
```

